﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace API.Middlewares
{
    /// <summary>
    /// 
    /// </summary>
    public class RequestMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        /// <inheritdoc />
        public RequestMiddleware(
            RequestDelegate next
            , ILoggerFactory loggerFactory
        )
        {
            this._next = next;
            this._logger = loggerFactory.CreateLogger<RequestMiddleware>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpContext"></param>
        /// <param name="metricReporting"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext httpContext, MetricReporting metricReporting)
        {
            var path = httpContext.Request.Path.Value;
            var method = httpContext.Request.Method;
            
            var sw = Stopwatch.StartNew();

            try
            {
                await _next.Invoke(httpContext);
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, $"The request to {path} is not performed");
            }

            if (path != "/metrics")
            {
                sw.Stop();
                metricReporting.RegisterRequest(path, method, httpContext.Response.StatusCode);
                metricReporting.RegisterResponseTime(httpContext.Response.StatusCode, method, path, sw.Elapsed);
            }
        }

    }
}
