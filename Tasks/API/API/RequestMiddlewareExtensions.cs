﻿using API.Middlewares;
using Microsoft.AspNetCore.Builder;

namespace API
{
    /// <summary>
    /// 
    /// </summary>
    public static class RequestMiddlewareExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseRequestMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<RequestMiddleware>();
        }
    }
}
