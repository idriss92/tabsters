﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using INFRASTRUCTURE.Entities;

namespace API.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ProjectTaskModel
    {
        [Required]
        [MaxLength(60)]
        public string Name { get; set; }
        [Required]
        public string ProjectManagerId { get; set; }
        [Required]
        public string AssigneeId { get; set; }
        [MaxLength(128)]
        public string Description { get; set; }
        [Required]
        [DefaultValue(ProjectTaskStatus.Plan)]
        public ProjectTaskStatus Status { get; set; }
        public DateTime CreateDateUtc { get; set; }
        public int EstimatedWorkingDay { get; set; }
    }
}
