﻿using System.ComponentModel.DataAnnotations;
using INFRASTRUCTURE.Auth;

namespace API.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class RegisterModel
    {
        /// <summary>
        /// 
        /// </summary>
        [Required]
        [EmailAddress]
        public string UserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Role Role { get; set; }
    }
}
