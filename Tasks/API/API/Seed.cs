﻿using System;
using System.Linq;
using System.Threading.Tasks;
using INFRASTRUCTURE;
using INFRASTRUCTURE.Auth;
using INFRASTRUCTURE.Entities;
using INFRASTRUCTURE.Extensions;
using INFRASTRUCTURE.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace API
{
    /// <summary>
    /// Seed to populate database at the start
    /// </summary>
    public class Seed
    {
        /// <summary>
        /// Use to populate database at launch of application
        /// </summary>
        /// <param name="context"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        public async Task SeedAsync(DataContext context, IServiceProvider provider)
        {
            var date1 = DateTime.UtcNow.AddMonths(1);
            var date3 = DateTime.UtcNow.AddMonths(3);
            var date4 = DateTime.UtcNow.AddMonths(4);
            var date5 = DateTime.UtcNow.AddMonths(5);
            var date6 = DateTime.UtcNow.AddMonths(6);

            if (!context.Roles.Any())
            {
                context.AddRange(new IdentityRole
                {
                    Name = Role.ProjectManager.GetRoleName(),
                    NormalizedName = Role.ProjectManager.GetRoleName().ToLower()
                }, new IdentityRole
                {
                    Name = Role.TaskDoer.GetRoleName(),
                    NormalizedName = Role.TaskDoer.GetRoleName().ToLower()
                });
            }

            context.SaveChanges();
            if (!context.Users.Any()) { 
                var auth = provider.GetRequiredService<AuthRepository>();
            var authResult1 =
                (auth.Register(
                    new ApplicationUser
                    {
                        FirstName = "Franck", LastName = "Beaucuse", UserName = "franck.beaucuse@yahoo.fr",
                        Email = "franck.beaucuse@yahoo.fr", EmailConfirmed = true
                    }, "Password1_")).Result;
            var authResult2 =
                (auth.Register(
                    new ApplicationUser
                    {
                        FirstName = "Paul", LastName = "Beaucuse", UserName = "paul.beaucuse@yahoo.fr",
                        Email = "paul.beaucuse@yahoo.fr", EmailConfirmed = true
                    }, "Password1_")).Result;
            var assignee = await auth.FindUserByMail("franck.beaucuse@yahoo.fr");
            var projectManager = await auth.FindUserByMail("paul.beaucuse@yahoo.fr");
            var addingRole1 = await auth.AddRoleToUser(projectManager, Role.ProjectManager.GetRoleName());
            var addingRole2 = await auth.AddRoleToUser(assignee, Role.TaskDoer.GetRoleName());

            if (!context.ProjectTasks.Any())
                context.AddRange(new ProjectTask
                {
                    AssigneeId = assignee.Id,
                    Description = "Project description",
                    Name = "task 1",
                    ProjectManagerId = projectManager.Id,
                    Status = ProjectTaskStatus.Plan,
                    CreateDateUtc = DateTime.UtcNow,
                    UpdateDateUtc = date3,
                    EstimatedWorkingDay = 4
                }, new ProjectTask
                {
                    AssigneeId = assignee.Id,
                    Description = "Project description",
                    Name = "task 2",
                    ProjectManagerId = projectManager.Id,
                    Status = ProjectTaskStatus.Plan,
                    CreateDateUtc = date1,
                    UpdateDateUtc = date4,
                    EstimatedWorkingDay = 4
                }, new ProjectTask
                {
                    AssigneeId = assignee.Id,
                    Description = "Project description",
                    Name = "task 3",
                    ProjectManagerId = projectManager.Id,
                    Status = ProjectTaskStatus.Plan,
                    CreateDateUtc = date5,
                    UpdateDateUtc = date6,
                    EstimatedWorkingDay = 4
                });

            await context.SaveChangesAsync();
            }
        }
    }
}
