﻿using INFRASTRUCTURE;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build()
                .MigrateDbContext<DataContext>((context, provider) =>
                {
                    new Seed()
                        .SeedAsync(context, provider)
                        .Wait();
                })
                .Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.ConfigureAppConfiguration((builderContext, config) =>
                    {
                        var builder = new ConfigurationBuilder()
                            .SetBasePath(builderContext.HostingEnvironment.ContentRootPath)
                            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                            .AddJsonFile($"appsettings.{builderContext.HostingEnvironment.EnvironmentName}.json", reloadOnChange: true,
                                optional: true);
                        builder.AddEnvironmentVariables();
                        config.AddConfiguration(builder.Build());
                    });
                    webBuilder.ConfigureLogging((hostingContext, builder) =>
                    {
                        builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                        builder.AddConsole();
                        builder.AddDebug();
                        builder.AddEventSourceLogger();
                    });
                    
                });

    }
}
