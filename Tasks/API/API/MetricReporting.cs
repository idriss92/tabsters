﻿using System;
using Microsoft.Extensions.Logging;
using Prometheus;

namespace API
{
    /// <summary>
    /// 
    /// </summary>
    public class MetricReporting
    {
        private readonly Counter _requestCounter;
        private readonly Histogram _responseTimeHistogram;

        /// <inheritdoc />
        public MetricReporting(ILogger<MetricReporting> logger)
        {
            _requestCounter =
                Metrics.CreateCounter("prometheus_tabsters_request_total", "HTTP Requests Total.", new CounterConfiguration
                {
                    LabelNames = new[] { "path", "method", "status" }
                });

            _responseTimeHistogram = Metrics.CreateHistogram("prometheus_tabsters_duration_seconds",
                "HTTP Requests duration.", new HistogramConfiguration
                {
                    Buckets = Histogram.ExponentialBuckets(0.01, 2, 10),
                    LabelNames = new[] { "path", "method", "status" }
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="method"></param>
        /// <param name="statusCode"></param>
        public void RegisterRequest(string path, string method, int statusCode)
        {
            _requestCounter.Labels(path, method, statusCode.ToString()).Inc();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="statusCode"></param>
        /// <param name="method"></param>
        /// <param name="path"></param>
        /// <param name="elapsed"></param>
        public void RegisterResponseTime(int statusCode, string method, string path, TimeSpan elapsed)
        {
            _responseTimeHistogram.Labels(path, method, statusCode.ToString()).Observe(elapsed.TotalSeconds);
        }
    }
}
