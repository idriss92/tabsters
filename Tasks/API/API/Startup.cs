﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using APPLICATION.Commands;
using APPLICATION.Repositories;
using INFRASTRUCTURE;
using INFRASTRUCTURE.Auth;
using INFRASTRUCTURE.HealthChecks;
using INFRASTRUCTURE.Repositories;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Prometheus;
using Serilog;
using Serilog.Exceptions;
using Serilog.Sinks.Elasticsearch;

namespace API
{
    /// <summary>
    /// 
    /// </summary>
    public class Startup
    {
        /// <inheritdoc />
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            var elasticUri = Configuration["ElasticConfigurationUri"];
            
            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .Enrich.WithExceptionDetails()
                .Enrich.WithMachineName()
                .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri(elasticUri))
                {
                    AutoRegisterTemplate = true,
                })
                .CreateLogger();
        }

        /// <summary>
        /// 
        /// </summary>
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHealthChecks();
            services
                .AddMvc(options => options.EnableEndpointRouting = false)
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
                .AddNewtonsoftJson(opt => opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);
            services.Configure<ContextSettings>(Configuration);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Tasks API", Version = "v1" });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            ConfigureDataProtection(services);
            ConfigureHealthCheckElements(services);
            ConfigureDependencies(services);
            ConfigureAuthentication(services);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddSerilog();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Tasks API V1");
            });
            app.UseHttpsRedirection();
            ConfigureReportingMetrics(app);

                app.UseAuthentication();
            app.UseMvc();


            app.UseRouting();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapHealthChecks("/healthCheck", new HealthCheckOptions
            {
                ResponseWriter = async (context, report) =>
                {
                    var response = new HealthCheckResponse
                    {
                        Status = report.Status.ToString(),
                        Checks = report.Entries.Select(x => new HealthCheck
                        {
                            Component = x.Key,
                            Status = x.Value.Status.ToString(),
                            Description = x.Value.Description
                        }),
                        Duration = report.TotalDuration
                    };
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(response));
                },
                ResultStatusCodes =
                {
                    [HealthStatus.Healthy] = StatusCodes.Status200OK,
                    [HealthStatus.Degraded] = StatusCodes.Status500InternalServerError,
                    [HealthStatus.Unhealthy] = StatusCodes.Status503ServiceUnavailable
                }
            });
        });

        }

        private void ConfigureDependencies(IServiceCollection services)
        {
            services.AddDbContext<DataContext>(c =>
            {
                try
                {
                    var settings = services.BuildServiceProvider().GetService<IOptions<ContextSettings>>().Value;
                    c.UseSqlServer(settings.ConnectionString);
                }
                catch (System.Exception ex)
                {
                    var message = ex.Message;
                }
            });

            services.AddIdentity<ApplicationUser, IdentityRole>(options =>
                {
                    options.User.RequireUniqueEmail = true;
                    
                })
                .AddRoleManager<RoleManager<IdentityRole>>()
                .AddEntityFrameworkStores<DataContext>()
                .AddDefaultTokenProviders();
            services.AddTransient<UserManager<ApplicationUser>, UserManager<ApplicationUser>>();
            services.AddTransient<SignInManager<ApplicationUser>, SignInManager<ApplicationUser>>();
            services.AddTransient<RoleManager<IdentityRole>, RoleManager<IdentityRole>>();
            services.AddSingleton<MetricReporting>();
            services.AddTransient<AuthRepository, AuthRepository>();
            services.AddTransient<IProjectTaskReadOnlyRepository, ProjectTaskRepository>();
            services.AddTransient<IProjectTaskWriteOnlyRepository, ProjectTaskRepository>();
            services.AddTransient<IPeopleReadOnlyRepository, PeopleRepository>();

            services.AddTransient<IRetrieveProjectTaskUseCase, RetriveProjectTaskUseCase>();
            services.AddTransient<IInsertProjectTaskUseCase, InsertProjectTaskUseCase>();
            services.AddTransient<IRetrieveOverlapReportUseCase, RetrieveOverlapReportUseCase>();
        }

        private void ConfigureHealthCheckElements(IServiceCollection services)
        {
            var settings = services.BuildServiceProvider().GetService<IOptions<ContextSettings>>().Value;
            services.AddHealthChecks()
                .AddSqlServer(settings.ConnectionString)
                .AddDbContextCheck<DataContext>();
        }

        private void ConfigureReportingMetrics(IApplicationBuilder application)
        {
            application.UseMetricServer();
            application.UseRequestMiddleware();
        }

        private void ConfigureDataProtection(IServiceCollection services)
        {
            services
                .AddDataProtection()
                .SetDefaultKeyLifetime(TimeSpan.FromDays(30))
                .SetApplicationName("Tabsters")
                ;
        }

        void ConfigureAuthentication(IServiceCollection services)
        {
            var settings = services.BuildServiceProvider().GetService<IOptions<ContextSettings>>().Value;

                services.AddAuthentication(x =>
                    {
                        x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                        x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    })
                    .AddJwtBearer(x =>
                    {
                        x.Events = new JwtBearerEvents
                        {
                            OnTokenValidated = context =>
                            {
                                var dataContext = context.HttpContext.RequestServices.GetService<DataContext>();
                                var userId = context.Principal.Identity.Name;
                                var user = dataContext.Users.FirstOrDefault(x => x.Id == userId.ToString());
                                if (user == null)
                                {
                                    context.Fail("UnAuthorized");
                                }

                                return Task.CompletedTask;
                            }
                        };
                        x.SaveToken = true;
                        x.TokenValidationParameters = new TokenValidationParameters
                        {
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(settings.SecretKey)),
                            ValidateIssuerSigningKey = true,
                            ValidateIssuer = false,
                            ValidateAudience = false
                        };
                    });

        }
    }
}
