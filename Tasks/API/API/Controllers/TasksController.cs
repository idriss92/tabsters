﻿using System;
using System.Threading.Tasks;
using API.Models;
using APPLICATION.Commands;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace API.Controllers
{
    /// <summary>
    /// Tasks management
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "ProjectManager")]
    public class TasksController : Controller
    {

        private readonly IInsertProjectTaskUseCase _insertProjectTaskUseCase;
        private readonly IRetrieveProjectTaskUseCase _retrieveProjectTaskUseCase;
        private readonly ILogger<TasksController> _logger;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="insertProjectTaskUseCase"></param>
        /// <param name="retrieveProjectTaskUseCase"></param>
        /// <param name="logger"></param>
        public TasksController(IInsertProjectTaskUseCase insertProjectTaskUseCase, IRetrieveProjectTaskUseCase retrieveProjectTaskUseCase, ILogger<TasksController> logger)
        {
            _insertProjectTaskUseCase = insertProjectTaskUseCase;
            _retrieveProjectTaskUseCase = retrieveProjectTaskUseCase;
            _logger = logger;
        }

        // GET api/tasks
        /// <summary>
        /// Retrieve all tasks
        /// </summary>
        /// <returns>List of Tasks</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> Get()
        {
            try
            {
                return Ok(await _retrieveProjectTaskUseCase.Execute());
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.ToString());
                return NoContent();
            }
        }

        // GET api/tasks/5
        /// <summary>
        /// Retrieve a task by id
        /// </summary>
        /// <param name="id">a task Id</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Get(string id)
        {
            try
            {
                var task = await _retrieveProjectTaskUseCase.Execute(id);
                _logger.LogInformation($"Retrievment of task with id: {id} succeed");
                return Ok(task);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Retrievment of task with id: {id} succeed");
                return NotFound(e.Message);
            }
        }

        // POST api/tasks
        /// <summary>
        /// Add a new task
        /// </summary>
        /// <param name="projectTask"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Post([FromBody] ProjectTaskModel projectTask)
        {
            _logger.LogInformation("insert in kibana");
            if (!ModelState.IsValid)
            {
                _logger.LogDebug($"Insert task {JsonConvert.SerializeObject(projectTask)} doesn't respect the requirement");
                _logger.LogInformation(ModelState.ToString());
                return BadRequest(ModelState);
            }
            try
            {
                await _insertProjectTaskUseCase.Execute(projectTask.Name, projectTask.Description,
                    projectTask.EstimatedWorkingDay, projectTask.ProjectManagerId, projectTask.AssigneeId);
                _logger.LogInformation($"Insert task {JsonConvert.SerializeObject(projectTask)} has been done");
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }

            return Ok();
        }
    }
}