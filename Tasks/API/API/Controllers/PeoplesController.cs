﻿using System.Linq;
using APPLICATION.Commands;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace API.Controllers
{
    /// <summary>
    /// Retrieve users reporting information
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "ProjectManager")]

    public class PeoplesController : Controller
    {
        private readonly IRetrieveOverlapReportUseCase _retrieveOverlapReportUseCase;
        private readonly ILogger<PeoplesController> _logger;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="retrieveOverlapReportUseCase"></param>
        /// <param name="logger"></param>
        public PeoplesController(IRetrieveOverlapReportUseCase retrieveOverlapReportUseCase, ILogger<PeoplesController> logger)
        {
            _retrieveOverlapReportUseCase = retrieveOverlapReportUseCase;
            _logger = logger;
        }
        
        // GET api/peoples
        /// <summary>
        /// Acces to reporting on all users
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public IActionResult Get()
        {
            var reporting = _retrieveOverlapReportUseCase.Execute();
            if (!reporting.Any())
            {
                _logger.LogInformation("There is no overlapping reporting");
                return NoContent();
            }
            _logger.LogInformation($"{reporting.Count()} reporting has been found");
            return Ok(reporting);
        }
    }
}