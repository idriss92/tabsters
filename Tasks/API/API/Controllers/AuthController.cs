﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using API.Models;
using APPLICATION.Results;
using INFRASTRUCTURE;
using INFRASTRUCTURE.Auth;
using INFRASTRUCTURE.Extensions;
using INFRASTRUCTURE.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace API.Controllers
{
    /// <summary>
    /// Authentication controller access to /api/Auth
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController: Controller
    {
        private readonly ILogger<AuthController> _logger;
        private readonly ContextSettings _contextSettings;
        private readonly AuthRepository _authManager;

        /// <inheritdoc />
        public AuthController(AuthRepository authManager, ILogger<AuthController> logger, IOptions<ContextSettings> appSettings)
        {
            _authManager = authManager;
            _logger = logger;
            _contextSettings = appSettings.Value;
        }


        /// <summary>
        /// Create account
        /// </summary>
        /// <param name="register"></param>
        /// <returns></returns>
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterModel register)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogTrace($"User doesn't submit right data, {JsonConvert.SerializeObject(ModelState.Values)}");
                return BadRequest();
            }

            var user = new ApplicationUser {Email = register.UserName, UserName = register.UserName};

            var result = await _authManager.Register(user, register.Password);
           
            if (!result.Succeeded)
            {
                return Unauthorized(result.Errors);
            }

            var res = await _authManager.AddRoleToUser(user, register.Role.GetRoleName());
            if (!res.Succeeded)
                return BadRequest(res.Errors);
            return Ok();
        }

        /// <summary>
        /// Retrieve token
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginModel login)
        {
            var time = DateTime.UtcNow.AddMinutes(15);
            var user = await _authManager.FindUserByMail(login.UserName);
            if (user == null)
                return NotFound(new {message = $"There is no user with {login}"});
            var result = await _authManager.Login(user, login.Password);
            var roles = await _authManager.GetUserRoles(user);
            if (!result.Succeeded)
            {
                return BadRequest(new {message = "Password is incorrect"});
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_contextSettings.SecretKey);
            var securityTokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, roles.FirstOrDefault() ) 
                }),
                Expires = time,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256Signature),
                IssuedAt = DateTime.UtcNow
            };

            var token = tokenHandler.CreateToken(securityTokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            return Ok(new TokenResult
            {
                Username = user.UserName,
                Token = tokenString,
                ExpiresAt= time
            });
        }
    }
}
