﻿using System.IO;
using INFRASTRUCTURE;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace API
{
    /// <summary>
    /// 
    /// </summary>
    public class ContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        /// <inheritdoc />
        public DataContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            var builder = new DbContextOptionsBuilder<DataContext>();
            var connectionString = configuration["ConnectionString"];
            builder.UseSqlServer(connectionString);

            return new DataContext(builder.Options);
        }
    }
}
