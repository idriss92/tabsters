﻿using System;
using System.Linq;

namespace APPLICATION.Results
{
    public class OverlapReport
    {
        public OverlapReport()
        {
            
        }

        public OverlapReport(PeopleResult peopleResult, ProjectTaskResult[] projectTask)
        {
            Tasks = 2;
            FirstName = peopleResult.FirstName;
            LastName = peopleResult.LastName;
            StartDateUtc = DateTime.Compare(projectTask.First().CreateDateUtc, projectTask.Last().CreateDateUtc) < 0 ? projectTask.Last().CreateDateUtc : projectTask.First().CreateDateUtc;
            EndDateUtc = DateTime.Compare(projectTask.First().UpdateDateUtc, projectTask.Last().UpdateDateUtc) > 0 ? projectTask.Last().UpdateDateUtc : projectTask.First().UpdateDateUtc;
            Tasks = projectTask.Length;
            Description = $"{FirstName} {LastName} a {Tasks} tâches du {StartDateUtc} au {EndDateUtc}";
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime StartDateUtc { get; set; }
        public DateTime EndDateUtc { get; set; }
        public int Tasks { get; set; }
        public string Description { get; set; }
    }
}
