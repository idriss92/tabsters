﻿using System.Collections.Generic;

namespace APPLICATION.Results
{
    public class PeopleResult
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<ProjectTaskResult> ProjectTasks { get; set; }
    }
}
