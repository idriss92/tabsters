﻿using System;

namespace APPLICATION.Results
{
    public class ProjectTaskResult
    {
        public string TaskId { get; set; }
        public string Name { get; set; }
        public string ProjectManagerId { get; set; }
        public string AssigneeId { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public DateTime CreateDateUtc { get; set; }
        public DateTime UpdateDateUtc { get; set; }
        public int EstimatedWorkingDay { get; set; }
        
    }
}
