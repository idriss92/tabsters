﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APPLICATION.Results
{
    public class TokenResult
    {
        public string Username { get; set; }
        public string Token { get; set; }
        public DateTime ExpiresAt { get; set; }
    }
}
