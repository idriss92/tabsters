﻿using System.Collections.Generic;
using System.Threading.Tasks;
using APPLICATION.Repositories;
using APPLICATION.Results;

namespace APPLICATION.Commands
{
    public sealed class RetriveProjectTaskUseCase: IRetrieveProjectTaskUseCase
    {
        private readonly IProjectTaskReadOnlyRepository _projectTaskReadOnlyRepository;


        public RetriveProjectTaskUseCase(IProjectTaskReadOnlyRepository projectTaskReadOnlyRepository)
        {
            _projectTaskReadOnlyRepository = projectTaskReadOnlyRepository;
        }
        public Task<ProjectTaskResult> Execute(string projectTaskId)
        {
            return _projectTaskReadOnlyRepository.GetProjectTaskById(projectTaskId);
        }

        public Task<IEnumerable<ProjectTaskResult>> Execute()
        {
            return _projectTaskReadOnlyRepository.GetProjectTasks();
        }
    }
}
