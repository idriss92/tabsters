﻿using System.Collections.Generic;
using System.Threading.Tasks;
using APPLICATION.Results;

namespace APPLICATION.Commands
{
    public interface IRetrieveProjectTaskUseCase
    {
        Task<ProjectTaskResult> Execute(string projectTaskId);
        Task<IEnumerable<ProjectTaskResult>> Execute();
    }
}
