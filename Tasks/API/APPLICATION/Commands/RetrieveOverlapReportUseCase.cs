﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APPLICATION.Repositories;
using APPLICATION.Results;

namespace APPLICATION.Commands
{
    public class RetrieveOverlapReportUseCase: IRetrieveOverlapReportUseCase
    {
        private readonly IPeopleReadOnlyRepository _peopleReadOnlyRepository;

        public RetrieveOverlapReportUseCase(IPeopleReadOnlyRepository peopleReadOnlyRepository)
        {
            _peopleReadOnlyRepository = peopleReadOnlyRepository;
        }

        public IEnumerable<OverlapReport> Execute()
        {
            var reporting = new List<OverlapReport>();
            var people = Task.Run(() => _peopleReadOnlyRepository.GetPeople()).Result;

            foreach (var p in people)
            {
                var concernedTasks = Overlappings(p.ProjectTasks).ToList();
                reporting.AddRange(concernedTasks.Select(t => new OverlapReport(p, t)));
            }

            return reporting;
        }

        private IEnumerable<ProjectTaskResult[]> Overlappings(List<ProjectTaskResult> projectTasks)
        {
            var first = (ProjectTaskResult)null;
            var orderedProjectTasks = projectTasks.OrderBy(m => m.CreateDateUtc).ToList();
            var checkedProjectTaskResults = new List<ProjectTaskResult>();
            foreach (var projectTask in orderedProjectTasks)
            {
                if (first != null)
                {
                    checkedProjectTaskResults.Add(first);

                    var uncheckedProjectTask = orderedProjectTasks.Where(x => !checkedProjectTaskResults.Contains(x) && (x.CreateDateUtc >= first.CreateDateUtc && x != first));
                    foreach (var projectTaskResult in uncheckedProjectTask)
                    {
                        if (first.UpdateDateUtc > projectTaskResult.CreateDateUtc)
                        {
                            yield return new ProjectTaskResult[] { first, projectTaskResult };
                        }
                    }
                }

                first = projectTask;
            }
        }
    }

}
