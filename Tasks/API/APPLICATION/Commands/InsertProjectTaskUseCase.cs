﻿using System;
using System.Threading.Tasks;
using APPLICATION.Repositories;

namespace APPLICATION.Commands
{
    public class InsertProjectTaskUseCase: IInsertProjectTaskUseCase
    {
        private readonly IProjectTaskWriteOnlyRepository _projectTaskWriteOnlyRepository;

        public InsertProjectTaskUseCase(IProjectTaskWriteOnlyRepository projectTaskWriteOnlyRepository)
        {
            _projectTaskWriteOnlyRepository = projectTaskWriteOnlyRepository;
        }

        public Task Execute(string name, string description, int estimatedWorkingDay, string creatorId, string assigneeId)
        {
            return _projectTaskWriteOnlyRepository.AddTask(name, description, estimatedWorkingDay, creatorId, assigneeId);
        }
    }
}
