﻿using System.Collections.Generic;
using APPLICATION.Results;

namespace APPLICATION.Commands
{
    public interface IRetrieveOverlapReportUseCase
    {
        IEnumerable<OverlapReport> Execute();
    }
}
