﻿using System;
using System.Threading.Tasks;

namespace APPLICATION.Commands
{
    public interface IInsertProjectTaskUseCase
    {
        Task Execute(string name, string description, int estimatedWorkingDay, string creatorId, string assigneeId);
    }
}
