﻿using System.Threading.Tasks;

namespace APPLICATION.Repositories
{
    public interface IProjectTaskWriteOnlyRepository
    {
        Task AddTask(string name, string description, int estimatedWorkingDay, string creatorId, string assigneeId);
    }
}
