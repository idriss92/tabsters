﻿using System.Collections.Generic;
using System.Threading.Tasks;
using APPLICATION.Results;

namespace APPLICATION.Repositories
{
    public interface IPeopleReadOnlyRepository
    {
        Task<IEnumerable<PeopleResult>> GetPeople();
    }
}
