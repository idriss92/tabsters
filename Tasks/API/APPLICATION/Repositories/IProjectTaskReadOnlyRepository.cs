﻿using System.Collections.Generic;
using System.Threading.Tasks;
using APPLICATION.Results;

namespace APPLICATION.Repositories
{
    public interface IProjectTaskReadOnlyRepository
    {
        Task<IEnumerable<ProjectTaskResult>> GetProjectTasks();
        Task<ProjectTaskResult> GetProjectTaskById(string projectTaskId);
    }
}
