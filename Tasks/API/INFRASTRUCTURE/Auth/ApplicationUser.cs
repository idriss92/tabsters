﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using INFRASTRUCTURE.Entities;
using Microsoft.AspNetCore.Identity;

namespace INFRASTRUCTURE.Auth
{
    public class ApplicationUser : IdentityUser
    {
        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }
        [Required]
        public string FirstName { get; set; }
        public ICollection<ProjectTask> ProjectTasks { get; set; }
    }
}