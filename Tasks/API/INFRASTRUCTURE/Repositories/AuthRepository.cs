﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using INFRASTRUCTURE.Auth;
using Microsoft.AspNetCore.Identity;

namespace INFRASTRUCTURE.Repositories
{
    public class AuthRepository
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        public AuthRepository(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }


        public async Task<SignInResult> Login(ApplicationUser applicationUser, string password)
        {
            var time = DateTime.UtcNow.AddMinutes(15);
            return await _signInManager.PasswordSignInAsync(applicationUser, password, true, true);
        }

        public async Task<IList<string>> GetUserRoles(ApplicationUser user)
        {
            return await _userManager.GetRolesAsync(user);
        }


        public async Task<IdentityResult> Register(ApplicationUser applicationUser, string password)
        {
            return await _userManager.CreateAsync(applicationUser, password);
        }

        public async Task<IdentityResult> AddRoleToUser(ApplicationUser user, string role)
        {
            return await _userManager.AddToRoleAsync(user, role.ToLowerInvariant());
        }

        public async Task<ApplicationUser> FindUserByMail(string email)
        {
            return await _userManager.FindByEmailAsync(email);
        }
    }
}
