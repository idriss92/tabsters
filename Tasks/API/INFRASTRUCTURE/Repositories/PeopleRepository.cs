﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APPLICATION.Repositories;
using APPLICATION.Results;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.EntityFrameworkCore;

namespace INFRASTRUCTURE.Repositories
{
    public class PeopleRepository: IPeopleReadOnlyRepository
    {
        private readonly DataContext _dataContext;
        private readonly IDataProtector _dataProtector;

        public PeopleRepository(DataContext dataContext, IDataProtectionProvider dataProtectionProvider)
        {
            _dataContext = dataContext;
            _dataProtector = dataProtectionProvider.CreateProtector("Tasks.PeopleRepository");
        }

        public async Task<IEnumerable<PeopleResult>> GetPeople()
        {
            return _dataContext.Users
                .Include(p => p.ProjectTasks)
                .Where(p => p.ProjectTasks.Count() > 1)
                .Select(p => new PeopleResult
                {
                    FirstName = p.FirstName,
                    LastName = p.LastName,
                    ProjectTasks = p.ProjectTasks.Select(pro => new ProjectTaskResult
                    {
                        Name = pro.Name,
                        CreateDateUtc = pro.CreateDateUtc,
                        UpdateDateUtc = pro.UpdateDateUtc,
                        Description = pro.Description,
                        TaskId = _dataProtector.Protect(pro.TaskId.ToString()),
                        EstimatedWorkingDay = pro.EstimatedWorkingDay,
                        AssigneeId = _dataProtector.Protect(pro.AssigneeId.ToString()),
                        ProjectManagerId = _dataProtector.Protect(pro.ProjectManagerId.ToString()),
                        Status = (int)pro.Status
                    }).ToList()
                });

        }
    }
}
