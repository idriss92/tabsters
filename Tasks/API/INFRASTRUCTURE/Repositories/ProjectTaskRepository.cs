﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APPLICATION.Repositories;
using APPLICATION.Results;
using INFRASTRUCTURE.Entities;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.EntityFrameworkCore;

namespace INFRASTRUCTURE.Repositories
{
    public class ProjectTaskRepository: IProjectTaskReadOnlyRepository, IProjectTaskWriteOnlyRepository
    {
        private readonly IDataProtector _dataProtector;
        private readonly DataContext _dataContext;
        public ProjectTaskRepository(DataContext dataContext, IDataProtectionProvider dataProtectionProvider)
        {
            _dataContext = dataContext;
            _dataProtector = dataProtectionProvider.CreateProtector("Tasks.ProjectTaskRepository");
        }

        public async Task<IEnumerable<ProjectTaskResult>> GetProjectTasks()
        {
            if (await _dataContext.ProjectTasks.CountAsync() > 0)
            {
                return (await _dataContext.ProjectTasks.ToListAsync()).Select(p => new ProjectTaskResult
                {
                    Name = p.Name,
                    CreateDateUtc = p.CreateDateUtc,
                    Description = p.Description,
                    TaskId = _dataProtector.Protect(p.TaskId.ToString()),
                    EstimatedWorkingDay = p.EstimatedWorkingDay,
                    AssigneeId = _dataProtector.Protect(p.AssigneeId.ToString()),
                    ProjectManagerId = _dataProtector.Protect(p.ProjectManagerId.ToString()),
                    Status = (int)p.Status
                });
                   
            }
            throw new Exception($"No data available");

        }

        public async Task<ProjectTaskResult> GetProjectTaskById(string projectTaskId)
        {
            var taskId = int.Parse(_dataProtector.Unprotect(projectTaskId));
            var task = await _dataContext.ProjectTasks.SingleOrDefaultAsync(p => p.TaskId == taskId);
            if (task == null)
            {
                throw new Exception($"Unable to retrieve data for {projectTaskId}");
            }

            return new ProjectTaskResult
            {
                Name = task.Name,
                CreateDateUtc = task.CreateDateUtc,
                Description = task.Description,
                TaskId = _dataProtector.Protect(task.TaskId.ToString()),
                EstimatedWorkingDay = task.EstimatedWorkingDay,
                AssigneeId = _dataProtector.Protect(task.AssigneeId.ToString()),
                ProjectManagerId = _dataProtector.Protect(task.ProjectManagerId.ToString()),
                Status = (int)task.Status
            };
        }

        public async Task AddTask(string name, string description, int estimatedWorkingDay, string creatorId, string assigneeId)
        {
            var existingTask = _dataContext.ProjectTasks.FirstOrDefault(x => x.Name == name);
            if (existingTask == null)
            {
                _dataContext.ProjectTasks.Add(new ProjectTask
                {
                    AssigneeId = assigneeId.ToString(),
                    CreateDateUtc = DateTime.UtcNow,
                    Description = description,
                    Name = name,
                    ProjectManagerId = creatorId
                });
                await _dataContext.SaveChangesAsync();
            }

            else
            {
                throw new Exception($"A project task with {name} already exists");
            }
        }
    }
}
