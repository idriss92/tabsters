﻿namespace INFRASTRUCTURE.Entities
{
    public enum ProjectTaskStatus
    {
        Plan = 0,
        Active = 1,
        InProgress = 2,
        Done = 3
    }
}
