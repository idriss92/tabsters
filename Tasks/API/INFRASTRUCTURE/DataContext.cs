﻿using INFRASTRUCTURE.Auth;
using INFRASTRUCTURE.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
namespace INFRASTRUCTURE
{
    public class DataContext : IdentityDbContext<ApplicationUser>
    {
        public DataContext(DbContextOptions options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ApplicationUser>().HasMany(p => p.ProjectTasks);
            modelBuilder.Entity<ProjectTask>().HasOne(x => x.Assignee);
        }

        public DbSet<ProjectTask> ProjectTasks { get; set; }
    }
}
