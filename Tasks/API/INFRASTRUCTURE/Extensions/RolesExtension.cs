﻿using INFRASTRUCTURE.Auth;

namespace INFRASTRUCTURE.Extensions
{
    public static class RolesExtension
    {
        public static string GetRoleName(this Role role) =>  role.ToString();
    }
}
