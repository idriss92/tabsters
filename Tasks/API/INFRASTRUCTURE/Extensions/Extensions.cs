﻿using Microsoft.EntityFrameworkCore;

namespace INFRASTRUCTURE.Extensions
{
    public static class EntityExtensions
    {
        public static void Clear<T>(this DbSet<T> dbSet) where T : class
        {
            dbSet.RemoveRange(dbSet);
        }
    }
}
