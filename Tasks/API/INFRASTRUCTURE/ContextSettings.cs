﻿namespace INFRASTRUCTURE
{
    public class ContextSettings
    {
        public string ConnectionString { get; set; }
        public string ElasticConfigurationUri { get; set; }
        public string SecretKey { get; set; }
    }
}
