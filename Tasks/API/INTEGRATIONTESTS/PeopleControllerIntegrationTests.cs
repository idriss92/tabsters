﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using API;
using API.Models;
using APPLICATION.Results;
using INFRASTRUCTURE;
using INFRASTRUCTURE.Auth;
using INFRASTRUCTURE.Entities;
using INFRASTRUCTURE.Extensions;
using INFRASTRUCTURE.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Xunit;

namespace INTEGRATIONTESTS
{
    public class PeopleControllerIntegrationTests: IClassFixture<TestingWebAppFactory<Startup>>
    {
        private readonly TestingWebAppFactory<Startup> _factory;

        public PeopleControllerIntegrationTests(TestingWebAppFactory<Startup> factory)
        {
            _factory = factory;
        }
  
        [Theory]
        [InlineData("/api/peoples", "/api/Auth/login")]
        public async Task Get_ReportingIsEmpty_ReturnNoContent(string url, string authorizedUrl)
        {
            var client = _factory.CreateClient();
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<DataContext>();
                var auth = scope.ServiceProvider.GetRequiredService<AuthRepository>();
                var rolesManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
                var role1 = rolesManager.CreateAsync(new IdentityRole
                {
                    Name = Role.ProjectManager.GetRoleName(),
                    NormalizedName = Role.ProjectManager.GetRoleName().ToLower()
                }).Result;
                var role2 = (rolesManager.CreateAsync(new IdentityRole
                {
                    Name = Role.TaskDoer.GetRoleName(),
                    NormalizedName = Role.TaskDoer.GetRoleName().ToLower()
                })).Result;

                context.ProjectTasks.Clear();
                context.Users.Clear();
                context.SaveChanges();
                var authResult = (auth.Register(new ApplicationUser { FirstName = "Paul", LastName = "Beaucuse", UserName = "paul.beaucuse@yahoo.fr", Email = "paul.beaucuse@yahoo.fr", EmailConfirmed = true }, "Password1_")).Result;
                var user = await auth.FindUserByMail("paul.beaucuse@yahoo.fr");
                var addingRole = await auth.AddRoleToUser(user, Role.ProjectManager.GetRoleName());
                var tokenInfo = (client.PostAsync(authorizedUrl,
                    new StringContent(
                        JsonConvert.SerializeObject(new LoginModel { UserName = user.UserName, Password = "Password1_" }),
                        Encoding.UTF8, "application/json"))).Result.Content.ReadAsStringAsync().Result;
                var token = JsonConvert.DeserializeObject<TokenResult>(tokenInfo).Token;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
     
            var response = await client.GetAsync(url);

            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
        }

        [Theory]
        [InlineData("/api/peoples", "/api/Auth/login")]
        public async Task Get_ReportingIsNotEmpty_ReturnOk(string url, string authorizedUrl)
        {
            var client = _factory.CreateClient();
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<DataContext>();
                var auth = scope.ServiceProvider.GetRequiredService<AuthRepository>(); var rolesManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
                var role1 = rolesManager.CreateAsync(new IdentityRole
                {
                    Name = Role.ProjectManager.GetRoleName(),
                    NormalizedName = Role.ProjectManager.GetRoleName().ToLower()
                }).Result;
                var role2 = (rolesManager.CreateAsync(new IdentityRole
                {
                    Name = Role.TaskDoer.GetRoleName(),
                    NormalizedName = Role.TaskDoer.GetRoleName().ToLower()
                })).Result;

                context.ProjectTasks.Clear();
                context.Users.Clear();
                context.SaveChanges();
                var authResult = (auth.Register(new ApplicationUser {FirstName = "Paul", LastName = "Beaucuse", UserName = "paul.beaucuse@yahoo.fr", Email = "paul.beaucuse@yahoo.fr", EmailConfirmed = true}, "Password1_")).Result;
                var user = await auth.FindUserByMail("paul.beaucuse@yahoo.fr");
                var addingRole = await auth.AddRoleToUser(user, Role.ProjectManager.GetRoleName());
                var tokenInfo = (client.PostAsync(authorizedUrl,
                    new StringContent(
                        JsonConvert.SerializeObject(new LoginModel { UserName = user.UserName, Password = "Password1_" }),
                        Encoding.UTF8, "application/json"))).Result.Content.ReadAsStringAsync().Result;
                var token = JsonConvert.DeserializeObject<TokenResult>(tokenInfo).Token;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token); var date = DateTime.UtcNow;
                context.AddRange(new []{new ProjectTask
                {
                    CreateDateUtc = date,
                    AssigneeId = context.Users.First().Id,
                    UpdateDateUtc = date.AddMonths(1),
                    Assignee = context.Users.First(),
                    Description = "Description",
                    Name = "First task",
                    ProjectManagerId = Guid.NewGuid().ToString(),
                    EstimatedWorkingDay = 2
                }, new ProjectTask
                {

                    CreateDateUtc = date.AddDays(3),
                    AssigneeId = context.Users.First().Id,
                    Assignee = context.Users.First(),
                    UpdateDateUtc = date.AddMonths(2),
                    Description = "Description",
                    Name = "second task",
                    ProjectManagerId = Guid.NewGuid().ToString(),
                    EstimatedWorkingDay = 2
                }});
                context.SaveChanges();
            }

            var response = await client.GetAsync(url);

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
    }
}
