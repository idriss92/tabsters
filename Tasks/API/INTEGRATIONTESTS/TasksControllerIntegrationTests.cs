﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using API;
using API.Models;
using APPLICATION.Results;
using INFRASTRUCTURE;
using INFRASTRUCTURE.Auth;
using INFRASTRUCTURE.Entities;
using INFRASTRUCTURE.Extensions;
using INFRASTRUCTURE.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Xunit;

namespace INTEGRATIONTESTS
{
    public class TasksControllerIntegrationTests: IClassFixture<TestingWebAppFactory<Startup>>
    {
        private readonly TestingWebAppFactory<Startup> _factory;


        public TasksControllerIntegrationTests(TestingWebAppFactory<Startup> factory)
        {
            _factory = factory;
        }


        [Theory]
        [InlineData("/api/tasks","/api/Auth/login")]
        public async Task Post_ObjectNotValid_ReturnBadRequest(string url, string authorizedUrl)
        {
            var client = _factory.CreateClient();
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<DataContext>();
                var auth = scope.ServiceProvider.GetRequiredService<AuthRepository>();
                var rolesManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
                var role1 = rolesManager.CreateAsync(new IdentityRole
                {
                    Name = Role.ProjectManager.GetRoleName(),
                    NormalizedName = Role.ProjectManager.GetRoleName().ToLower()
                }).Result;
                var role2 = (rolesManager.CreateAsync(new IdentityRole
                {
                    Name = Role.ProjectManager.GetRoleName(),
                    NormalizedName = Role.TaskDoer.GetRoleName().ToLower()
                })).Result;

                context.ProjectTasks.Clear();
                context.Users.Clear();
                context.SaveChanges();
                var authResult = (auth.Register(new ApplicationUser { FirstName = "Paul", LastName = "Beaucuse", UserName = "paul.beaucuse@yahoo.fr", Email = "paul.beaucuse@yahoo.fr", EmailConfirmed = true }, "Password1_")).Result;
                var user = await auth.FindUserByMail("paul.beaucuse@yahoo.fr");
                var addingRole = await auth.AddRoleToUser(user, Role.ProjectManager.GetRoleName());
                var tokenInfo = (client.PostAsync(authorizedUrl,
                    new StringContent(
                        JsonConvert.SerializeObject(new LoginModel {UserName = user.UserName, Password = "Password1_"}),
                        Encoding.UTF8, "application/json"))).Result.Content.ReadAsStringAsync().Result;
                var token = JsonConvert.DeserializeObject<TokenResult>(tokenInfo).Token;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            var projectTask = new ProjectTask();

            var response = await client.PostAsync(url, new StringContent(JsonConvert.SerializeObject(projectTask),Encoding.UTF8,"application/json"));

            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Theory]
        [InlineData("/api/tasks", "/api/Auth/login")]
        public async Task Post_ObjectValid_ReturnOk(string url, string authorizedUrl)
        {
            var client = _factory.CreateClient();
            string userId;
            ApplicationUser assignee;
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<DataContext>();
                var auth = scope.ServiceProvider.GetRequiredService<AuthRepository>();
                var rolesManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
                var role1 = rolesManager.CreateAsync(new IdentityRole
                {
                    Name = Role.ProjectManager.GetRoleName(),
                    NormalizedName = Role.ProjectManager.GetRoleName().ToLower()
                }).Result;
                var role2 = (rolesManager.CreateAsync(new IdentityRole
                {
                    Name = Role.TaskDoer.GetRoleName(),
                    NormalizedName = Role.TaskDoer.GetRoleName().ToLower()
                })).Result;

                context.ProjectTasks.Clear();
                context.Users.Clear();
                context.SaveChanges();
                var authResult1 = (auth.Register(new ApplicationUser { FirstName = "Franck", LastName = "Beaucuse", UserName = "franck.beaucuse@yahoo.fr", Email = "franck.beaucuse@yahoo.fr", EmailConfirmed = true }, "Password1_")).Result;
                var authResult2 = (auth.Register(new ApplicationUser { FirstName = "Paul", LastName = "Beaucuse", UserName = "paul.beaucuse@yahoo.fr", Email = "paul.beaucuse@yahoo.fr", EmailConfirmed = true }, "Password1_")).Result;
                assignee = await auth.FindUserByMail("franck.beaucuse@yahoo.fr");
                var user = await auth.FindUserByMail("paul.beaucuse@yahoo.fr");
                userId = user.Id;
                var addingRole1 = await auth.AddRoleToUser(user, Role.ProjectManager.GetRoleName());
                var addingRole2 = await auth.AddRoleToUser(assignee, Role.TaskDoer.GetRoleName());
                var tokenInfo = (client.PostAsync(authorizedUrl,
                    new StringContent(
                        JsonConvert.SerializeObject(new LoginModel { UserName = user.UserName, Password = "Password1_" }),
                        Encoding.UTF8, "application/json"))).Result.Content.ReadAsStringAsync().Result;
                var token = JsonConvert.DeserializeObject<TokenResult>(tokenInfo).Token;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            var projectTask = new ProjectTaskModel
            {
                CreateDateUtc = DateTime.UtcNow,
                Description = "Description",
                EstimatedWorkingDay = 2,
                Name = "Name",
                ProjectManagerId = userId,
                AssigneeId = assignee.Id
            };

            var response = await client.PostAsync(url, new StringContent(JsonConvert.SerializeObject(projectTask), Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Theory]
        [InlineData("/api/tasks", "/api/Auth/login")]
        public async Task Post_ObjectValid_ReturnInternalServerError(string url, string authorizedUrl)
        {
            var client = _factory.CreateClient();
            string userId;
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<DataContext>();
                var auth = scope.ServiceProvider.GetRequiredService<AuthRepository>();
                var rolesManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
                var role1 = rolesManager.CreateAsync(new IdentityRole
                {
                    Name = Role.ProjectManager.GetRoleName(),
                    NormalizedName = Role.ProjectManager.GetRoleName().ToLower()
                }).Result;
                var role2 = (rolesManager.CreateAsync(new IdentityRole
                {
                    Name = Role.TaskDoer.GetRoleName(),
                    NormalizedName = Role.TaskDoer.GetRoleName().ToLower()
                })).Result;

                context.ProjectTasks.Clear();
                context.Users.Clear();
                context.SaveChanges();
                var authResult2 = (auth.Register(new ApplicationUser { FirstName = "Paul", LastName = "Beaucuse", UserName = "paul.beaucuse@yahoo.fr", Email = "paul.beaucuse@yahoo.fr", EmailConfirmed = true }, "Password1_")).Result;
               var user = await auth.FindUserByMail("paul.beaucuse@yahoo.fr");
                userId = user.Id;
                var addingRole1 = await auth.AddRoleToUser(user, Role.ProjectManager.GetRoleName());
                var tokenInfo = (client.PostAsync(authorizedUrl,
                    new StringContent(
                        JsonConvert.SerializeObject(new LoginModel { UserName = user.UserName, Password = "Password1_" }),
                        Encoding.UTF8, "application/json"))).Result.Content.ReadAsStringAsync().Result;
                var token = JsonConvert.DeserializeObject<TokenResult>(tokenInfo).Token;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            var projectTask = new ProjectTaskModel
            {
                CreateDateUtc = DateTime.UtcNow,
                Description = "Description",
                EstimatedWorkingDay = 2,
                Name = "Name",
                ProjectManagerId = userId,
                AssigneeId = userId
            };

            var firstInsertion = await client.PostAsync(url, new StringContent(JsonConvert.SerializeObject(projectTask), Encoding.UTF8, "application/json"));
            var response = await client.PostAsync(url, new StringContent(JsonConvert.SerializeObject(projectTask), Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.InternalServerError, response.StatusCode);
        }

        [Theory]
        [InlineData("/api/tasks", "/api/Auth/login")]
        public async Task Get_ReturnNoContent(string url, string authorizedUrl)
        {
            var client = _factory.CreateClient();
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<DataContext>();
                var auth = scope.ServiceProvider.GetRequiredService<AuthRepository>();
                var rolesManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
                var role1 = rolesManager.CreateAsync(new IdentityRole
                {
                    Name = Role.ProjectManager.GetRoleName(),
                    NormalizedName = Role.ProjectManager.GetRoleName().ToLower()
                }).Result;
                var role2 = (rolesManager.CreateAsync(new IdentityRole
                {
                    Name = Role.ProjectManager.GetRoleName(),
                    NormalizedName = Role.TaskDoer.GetRoleName().ToLower()
                })).Result;

                context.ProjectTasks.Clear();
                context.Users.Clear();
                context.SaveChanges();
                var authResult = (auth.Register(new ApplicationUser { FirstName = "Paul", LastName = "Beaucuse", UserName = "paul.beaucuse@yahoo.fr", Email = "paul.beaucuse@yahoo.fr", EmailConfirmed = true }, "Password1_")).Result;
                var user = await auth.FindUserByMail("paul.beaucuse@yahoo.fr");
                var addingRole = await auth.AddRoleToUser(user, Role.ProjectManager.GetRoleName());
                var tokenInfo = (client.PostAsync(authorizedUrl,
                    new StringContent(
                        JsonConvert.SerializeObject(new LoginModel { UserName = user.UserName, Password = "Password1_" }),
                        Encoding.UTF8, "application/json"))).Result.Content.ReadAsStringAsync().Result;
                var token = JsonConvert.DeserializeObject<TokenResult>(tokenInfo).Token;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }

            var response = await client.GetAsync(url);

            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
        }

        [Theory]
        [InlineData("/api/tasks", "/api/Auth/login")]
        public async Task Get_ObjectValid_ReturnOk(string url, string authorizedUrl)
        {
            var client = _factory.CreateClient();
            string userId;
            ApplicationUser assignee;
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<DataContext>();
                var auth = scope.ServiceProvider.GetRequiredService<AuthRepository>();
                var rolesManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
                var role1 = rolesManager.CreateAsync(new IdentityRole
                {
                    Name = Role.ProjectManager.GetRoleName(),
                    NormalizedName = Role.ProjectManager.GetRoleName().ToLower()
                }).Result;
                var role2 = (rolesManager.CreateAsync(new IdentityRole
                {
                    Name = Role.TaskDoer.GetRoleName(),
                    NormalizedName = Role.TaskDoer.GetRoleName().ToLower()
                })).Result;

                context.ProjectTasks.Clear();
                context.Users.Clear();
                context.SaveChanges();
                var authResult1 = (auth.Register(new ApplicationUser { FirstName = "Franck", LastName = "Beaucuse", UserName = "franck.beaucuse@yahoo.fr", Email = "franck.beaucuse@yahoo.fr", EmailConfirmed = true }, "Password1_")).Result;
                var authResult2 = (auth.Register(new ApplicationUser { FirstName = "Paul", LastName = "Beaucuse", UserName = "paul.beaucuse@yahoo.fr", Email = "paul.beaucuse@yahoo.fr", EmailConfirmed = true }, "Password1_")).Result;
                assignee = await auth.FindUserByMail("franck.beaucuse@yahoo.fr");
                var user = await auth.FindUserByMail("paul.beaucuse@yahoo.fr");
                userId = user.Id;
                var addingRole1 = await auth.AddRoleToUser(user, Role.ProjectManager.GetRoleName());
                var addingRole2 = await auth.AddRoleToUser(assignee, Role.TaskDoer.GetRoleName());
                var tokenInfo = (client.PostAsync(authorizedUrl,
                    new StringContent(
                        JsonConvert.SerializeObject(new LoginModel { UserName = user.UserName, Password = "Password1_" }),
                        Encoding.UTF8, "application/json"))).Result.Content.ReadAsStringAsync().Result;
                var token = JsonConvert.DeserializeObject<TokenResult>(tokenInfo).Token;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            var projectTask = new ProjectTaskModel
            {
                CreateDateUtc = DateTime.UtcNow,
                Description = "Description",
                EstimatedWorkingDay = 2,
                Name = "Name",
                ProjectManagerId = userId,
                AssigneeId = assignee.Id
            };

            var res = Task.Run(() => client.PostAsync(url, new StringContent(JsonConvert.SerializeObject(projectTask), Encoding.UTF8, "application/json"))).Result;
            var response = await client.GetAsync(url);

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }


    }
}
